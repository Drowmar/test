package com.drowmar.myapplication.resources.data.remote

import com.google.gson.annotations.SerializedName

data class StarRemoteEntity (
    @SerializedName("id") val id: String,
    @SerializedName("ra") val ra: Float?,
    @SerializedName("de") val de: Float?,
    @SerializedName("category") val category: String,
    @SerializedName("distance") val distance: String?,
    @SerializedName("distanceUnit") val distanceUnit: String?,
    )