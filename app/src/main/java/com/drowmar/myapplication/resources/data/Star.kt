package com.drowmar.myapplication.resources.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Star (
    var id: String,
    var ra: Float?,
    var de: Float?,
    var category: String,
    var distance: String?,
    var distanceUnit: String?) : Parcelable
