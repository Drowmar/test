package com.drowmar.myapplication.resources.domain

import com.drowmar.myapplication.resources.data.Star
import com.drowmar.myapplication.resources.data.StarsRepository
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class UseCase (private val starsRepository: StarsRepository) {

    suspend fun getStars(): List<Star> = suspendCoroutine {
        starsRepository.getStars { list ->
            it.resume(list)
        }
    }

}