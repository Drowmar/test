package com.drowmar.myapplication.resources.data

import android.content.Context
import com.drowmar.myapplication.resources.data.remote.StarApi
import com.drowmar.myapplication.resources.data.remote.StarRemoteDataSource
import com.drowmar.myapplication.resources.data.remote.StarRemoteEntity
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface Repository {
     fun getStars(callback: (List<Star>) -> Unit)
}

class StarsRepository() : Repository {

    private val httpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()

    private val retrofit = Retrofit.Builder()
        .client(httpClient)
        .baseUrl("https://vaonis-back-prod-eu.herokuapp.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val starRemoteDataSource = StarRemoteDataSource(retrofit.create(StarApi::class.java))

    override fun getStars(callback: (List<Star>) -> Unit) {
        starRemoteDataSource.getStars2 {
            val stars = it.filter { it.de != null && it.ra != null && it.de > 0 }
                .map { it.mapToStar() }
            callback(stars)
        }

    }
}

fun StarRemoteEntity.mapToStar(): Star {
    return Star(this.id, this.ra, this.de, this.category, this.distance, this.distanceUnit)
}
