package com.drowmar.myapplication.resources.data.remote

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

interface StarApi {

    @GET("catalog")
    @Headers("Authorization: Token 060a0d44f1e4fa13ff7d6e849ed6afcccce298fd28f42d82870275f8c407b7f7")
    fun fetchStars() : Call<List<StarRemoteEntity>>
}