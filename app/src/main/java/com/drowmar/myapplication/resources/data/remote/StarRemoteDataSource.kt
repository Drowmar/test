package com.drowmar.myapplication.resources.data.remote

import com.drowmar.myapplication.resources.data.Star
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class StarRemoteDataSource(private val starApi: StarApi) {
     fun getStars() : Call<List<StarRemoteEntity>> =
         starApi.fetchStars()

    fun getStars2(callback: (List<StarRemoteEntity>) -> Unit) {
        starApi.fetchStars().enqueue(object : Callback<List<StarRemoteEntity>>{
            override fun onResponse(
                call: Call<List<StarRemoteEntity>>,
                response: Response<List<StarRemoteEntity>>) {
                response.body()?.let {
                    callback(it)
                }
            }

            override fun onFailure(call: Call<List<StarRemoteEntity>>, t: Throwable) {

            }

        })

    }
}
