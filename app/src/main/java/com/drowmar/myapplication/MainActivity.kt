package com.drowmar.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.drowmar.myapplication.resources.data.Star
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val viewModel: SkyMapViewModel by viewModels {
        SkyMapViewModel.Factory()
    }

    private lateinit var stars: List<Star>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
    }

    override fun onStart() {
        super.onStart()
        viewModel.state.observe(this, ::onStateChanged)
        viewModel.refreshStars()
        glview.starSelectedCallback = { showStarInfo(it) }
    }

    private fun onStateChanged(contactListState: StarListState): Unit =
        when (contactListState) {
            StarListState.Idle -> onIdleState()
            is StarListState.Loaded -> onLoadedState(contactListState)
        }

    private fun onIdleState() {}

    private fun showStarInfo(star: Star)  {
        info_name.text = star.id
        info_cate.text = "Category: " + star.category
        val distanceText = if (star.distance != null ) "Distance: " + star.distance  + " " + star.distanceUnit else "Unknown"
        info_distance.text = distanceText
        popup.visibility = View.VISIBLE
    }

    private fun closeStarInfo()  {
        popup.visibility = View.GONE
    }

    private fun changeMap(filter: String) {
        closeStarInfo()
        if (filter == "All") {
            glview.setStars(stars)
        } else {
            val selected = stars.filter { it.category.startsWith(filter.lowercase()) }
            glview.setStars(selected)
        }
    }

    private fun setupCategory() {
        if (tab.tabCount > 0) {
            return
        }
        val categories: MutableSet<String> = mutableSetOf("All")
        stars.forEach { categories.add(it.category.split("-")[0].replaceFirstChar(Char::uppercase)) }
        categories.forEach {
            val t = tab.newTab()
            t.text = it
            tab.addTab(t)
        }
        tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {}

            override fun onTabUnselected(p0: TabLayout.Tab?) {}

            override fun onTabSelected(p0: TabLayout.Tab?) {
                changeMap(p0?.text.toString())
            }
        })
        changeMap("All")
    }

    private fun onLoadedState(loaded: StarListState.Loaded) {
        stars = loaded.stars
        setupCategory()
    }
}
