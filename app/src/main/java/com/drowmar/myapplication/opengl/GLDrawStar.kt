package com.drowmar.myapplication.opengl

import com.drowmar.myapplication.resources.data.Star
import java.text.FieldPosition

class GLDrawStar(
     val star: Star,
     val cartesian: Pair<Float, Float>
)
