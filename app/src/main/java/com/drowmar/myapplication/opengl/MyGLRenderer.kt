package com.drowmar.myapplication.opengl

import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import com.drowmar.myapplication.resources.data.Star
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import kotlin.math.cos
import kotlin.math.sin

class MyGLRenderer(private val callback: ((List<GLDrawStar>) -> Unit)) : GLSurfaceView.Renderer {

    private lateinit var mGLStar: GLStar
    private lateinit var mGLCircle: GLCircle



    // vPMatrix is an abbreviation for "Model View Projection Matrix"
    private val vPMatrix = FloatArray(16)
    private val projectionMatrix = FloatArray(16)
    private val viewMatrix = FloatArray(16)
    private var mWidth: Int = 0
    private var mHeight: Int = 0
    private var idSelectedStar: String? = null
    private var mListOfStars: List<Star> = emptyList()


    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f)

        mGLStar = GLStar()
        mGLCircle = GLCircle()
    }

    fun polarToCartesian(ra: Float, de: Float) : Pair<Float, Float>
    {
        val deInterpolated: Float = (90.0f - de) / 90.0f

        // +90 rotates the radian scale by one quarter of a round
        // MINUS cos() mirrors the radian scale to match the required one.
        // Multiply the Y part by width because we contain it in a square. Should multiply both by the smallest if wanting to fully handle resize
        val ra2 = Math.toRadians(ra.toDouble() - 90.0).toFloat()
        return Pair(mWidth / 2.0f - deInterpolated * -cos(ra2) * mWidth / 2.0f, mHeight / 2.0f - deInterpolated * sin(ra2) * mWidth / 2.0f)
    }


    override fun onDrawFrame(unused: GL10) {
        // Redraw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)

        // Set the camera position (View matrix)
        Matrix.setLookAtM(viewMatrix, 0, 0f, 0f, 3f, 0f, 0f, 0f, 0f, 1.0f, 0.0f)

        // Calculate the projection and view transformation
        Matrix.multiplyMM(vPMatrix, 0, projectionMatrix, 0, viewMatrix, 0)

        mGLCircle.draw(vPMatrix, mWidth / 2.0f, mHeight / 2.0f, 0.0f)
        mGLCircle.draw(vPMatrix, mWidth / 2.0f, mHeight / 2.0f, mWidth / 8.0f)
        mGLCircle.draw(vPMatrix, mWidth / 2.0f, mHeight / 2.0f, mWidth / 3.3f)
        mGLCircle.draw(vPMatrix, mWidth / 2.0f, mHeight / 2.0f, mWidth / 2.1f)

        val drawStar = mutableListOf<GLDrawStar>()

        mListOfStars.forEach {
            val coord: Pair<Float, Float> = polarToCartesian(it.ra!!, it.de!!)
            val c : Pair<Float, Float> = Pair(coord.first, mHeight - coord.second)
            drawStar.add(GLDrawStar(it, c))
            if (it.id == idSelectedStar) {
                mGLStar.draw(vPMatrix, coord.first, coord.second, true)
            } else {
                mGLStar.draw(vPMatrix, coord.first, coord.second, false)

            }
        }
        callback.invoke(drawStar)
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        mWidth = width
        mHeight = height

        val ratio: Float = width.toFloat() / height.toFloat()

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1f, 1f, 3f, 7f)

    }

    fun setStars(stars: List<Star>) {
        mListOfStars = stars
    }

    fun setSelectedStar(id: String) {
        this.idSelectedStar = id
    }

}
