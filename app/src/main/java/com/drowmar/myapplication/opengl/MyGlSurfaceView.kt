package com.drowmar.myapplication.opengl

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import com.drowmar.myapplication.resources.data.Star

class MyGLSurfaceView(context: Context?, attrs: AttributeSet?) : GLSurfaceView(context, attrs) {

    private val renderer: MyGLRenderer

    private var currentStars = listOf<GLDrawStar>()

    lateinit var starSelectedCallback: ((Star) -> Unit)

    init {
        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2)

        renderer = MyGLRenderer { currentStars = it }

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(renderer)
        renderMode = RENDERMODE_WHEN_DIRTY
    }

    fun isTouching(x: Float, y: Float, cartesian: Pair<Float, Float>) : Boolean {
        return ((x - 30 < cartesian.first) && (x + 30 > cartesian.first) && (y - 30 < cartesian.second) && (y + 30 > cartesian.second))
    }

    override fun onTouchEvent(e: MotionEvent): Boolean {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        val x: Float = e.x
        val y: Float = e.y
        currentStars.forEach {
            if (isTouching(x, y, it.cartesian)) {
                setSelectedStar(it.star.id)
                starSelectedCallback(it.star)
                return true
            }
        }
        return true
    }

    fun setSelectedStar(id: String) {
        renderer.setSelectedStar(id)
        requestRender()
    }
    fun setStars(stars: List<Star>) {
        renderer.setStars(stars)
        requestRender()
    }

}
