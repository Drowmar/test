package com.drowmar.myapplication.opengl

import android.opengl.GLES20
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer


class GLCircle {

    private val drawOrder = shortArrayOf(0, 1, 2, 0, 2, 3) // order to draw vertices

    private var mProgram: Int = 0

    var vertexShaderCode = "" +
            "attribute vec2 a_Position; \n" +
            "uniform mat4 uMVPMatrix; \n" +
            "void main() \n" +
            "{ \n" +
            "  gl_Position = uMVPMatrix * vec4(a_Position, 0., 1.);" +
            "} \n"

    var fragmentShaderCode = "" +
            "precision highp float;\n" +
            "uniform vec2 aCirclePosition;\n" +
            "uniform float aRadius; \n" +
            "uniform vec4 vColor; \n" +
            "const float threshold = 2.0;\n" +

            "void main() \n" +
            "{ \n" +
            "   float d, dist, dist2;\n" +
            "   dist = distance(aCirclePosition, gl_FragCoord.xy);\n" +
            "   if(dist == 0.)\n" +
            "       dist = 1.;\n" +
            "   d = aRadius / dist;\n" +
            "   if (dist <= aRadius)\n" +
            "        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n" +
            "   else if(dist - threshold <= aRadius) \n" +
            "   {\n" +
            "        float a = (d - (1. - threshold)) / threshold;\n" +
            "        gl_FragColor = vec4(vColor.r, vColor.g, vColor.b, 1); \n" +
            "    }\n" +
            "    else\n" +
            "        gl_FragColor = vec4(0., 0., 0., 0.);\n" +
            "} \n"

    private fun loadShader(type: Int, shaderCode: String): Int {

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        return GLES20.glCreateShader(type).also { shader ->

            // add the source code to the shader and compile it
            GLES20.glShaderSource(shader, shaderCode)
            GLES20.glCompileShader(shader)
        }
    }

    private var vPMatrixHandle: Int = 0

    private val drawOrderBuffer: ShortBuffer = ByteBuffer.allocateDirect(drawOrder.size * 2).run {
        order(ByteOrder.nativeOrder())
        asShortBuffer().apply {
            put(drawOrder)
            position(0)
        }
    }
    private var mColorHandle: Int = 0

    private val COORDINATES_PER_VERTEX = 2
    private val VERTEX_STRIDE: Int = COORDINATES_PER_VERTEX * 4

    private val QUADRANT_COORDINATES = floatArrayOf(
        //x,    y
        -1f, 1f,
        -1f, -1f,
        1f, -1f,
        1f, 1f,
    )

    private val quadrantCoordinatesBuffer: FloatBuffer = ByteBuffer.allocateDirect(QUADRANT_COORDINATES.size * 4).run {
        order(ByteOrder.nativeOrder())
        asFloatBuffer().apply {
            put(QUADRANT_COORDINATES)
            position(0)
        }
    }

    fun draw(mvpMatrix: FloatArray, mX: Float, mY: Float, mRadius: Float) {

        val color = floatArrayOf(
            168.0f / 255.0f, 183.0f / 255.0f, 202.0f / 255.0f, 1.0f
        )

        // Add program to OpenGL ES environment
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glUseProgram(mProgram)

        var quadPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position")

        GLES20.glVertexAttribPointer(
            quadPositionHandle,
            COORDINATES_PER_VERTEX,
            GLES20.GL_FLOAT,
            false,
            VERTEX_STRIDE,
            quadrantCoordinatesBuffer
        )
        GLES20.glEnableVertexAttribArray(quadPositionHandle)
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor").also { colorHandle ->
            // Set color for drawing the triangle
            GLES20.glUniform4fv(colorHandle, 1, color, 0)
        }


        // get handle to shape's transformation matrix
        vPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(vPMatrixHandle, 1, false, mvpMatrix, 0)

        GLES20.glUniform2f(GLES20.glGetUniformLocation(mProgram, "aCirclePosition"), mX, mY);
        GLES20.glUniform1f(GLES20.glGetUniformLocation(mProgram, "aRadius"), mRadius);

        GLES20.glDrawElements(
            GLES20.GL_TRIANGLES,
            drawOrder.size,
            GLES20.GL_UNSIGNED_SHORT,
            drawOrderBuffer
        )

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(quadPositionHandle)
    }

    init {
        val vertexShader: Int = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader: Int = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram().also {

            // add the vertex shader to program
            GLES20.glAttachShader(it, vertexShader)

            // add the fragment shader to program
            GLES20.glAttachShader(it, fragmentShader)

            // creates OpenGL ES program executables
            GLES20.glLinkProgram(it)
        }
    }
}