package com.drowmar.myapplication

import android.content.Context
import androidx.lifecycle.*
import com.drowmar.myapplication.resources.data.Star
import com.drowmar.myapplication.resources.data.StarsRepository
import com.drowmar.myapplication.resources.domain.UseCase

import kotlinx.coroutines.launch

class SkyMapViewModel : ViewModel() {

    private val useCase = UseCase(StarsRepository())

    private val _state = MutableLiveData<StarListState>(StarListState.Idle)
    val state: LiveData<StarListState> get() = _state

    fun refreshStars() {
        viewModelScope.launch {
            val state = _state.value
            val t = useCase.getStars()
            val updatedList = when (state) {
                StarListState.Idle -> t
                is StarListState.Loaded ->  t
                null -> { listOf<Star>() }
            }
            _state.value = StarListState.Loaded(updatedList)
        }
    }

    class Factory() : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return SkyMapViewModel() as T
        }
    }
}

sealed class StarListState {
    object Idle : StarListState()
    class Loaded(val stars: List<Star>) : StarListState()
}